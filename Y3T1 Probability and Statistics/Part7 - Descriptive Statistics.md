<style>ol { list-style: hiragana-iroha; padding: 0 3em; }
ol.def { list-style: katakana-iroha; padding: 0 3em; }
ol li span.MathJax_Preview { display: none }</style>

Descriptive Statistics
======================

In most applications, we don't know the PMF/PDF,
but we may find out the probability distribution of the random variable. \(
  \newcommand{sumin}[0]{\sum_{i=1}^n}
  \newcommand{xbar}[0]{\bar{x}}
\)


### Statistical Inference

Perform an experiment, and record as a _data set_. From the data set,
we find the _probability distribution_, called __statistical inference__.

```
              Experiment/                   
Probability   Observation   Data Set           Data Analysis   Estimate
distribution  ------------> [ x1, x2, x3, .. ] --------------> properties of
[ f(x) ]      Probability                      Statistical     [ f(x) ]
              Theory                           Inference
```


### Definitions

<dl>
<dt>Population
<dd>All possible observations from probability distribution.
<dt>Sample
<dd>A subset of population, used to investigate the unknown probability distribution.
<dt>Random Sample
<dd>Randomly-chosen sample to be the representative.
</dl>


Sample Statistics
-----------------

<ol>
<li>Sample Mean
  $$ \bar{x} = \frac{\sumin x_i}{n} $$
<li>Sample Median
<li>Sample Trimmed Mean
<li>Sample Mode
<li>Sample Variance
  $$ s^2 = \frac{\sumin (x_i - \xbar)^2}{n - 1}
         = \frac{\sumin x_i^2 - n \xbar^2}{n - 1} $$
  (Sample Standard Deviation = \(s\))
<li>Sample Quartiles
<li>Coefficient of Variation
  $$ CV = \frac{s}{\xbar} $$
</ol>












