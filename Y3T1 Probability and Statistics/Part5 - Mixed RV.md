<style>ol { list-style: hiragana-iroha; padding: 0 3em; }
ol li span.MathJax_Preview { display: none }</style>

Mixed Random Variable
=====================

  * Combination of Discrete RV and Continuous RV
      * Unit impulse function\(
 
\newcommand{ii}[0]{\int_{-\infty}^\infty}
\newcommand{Var}[0]{\text{Var}}
\newcommand{E}[0]{\text{E}}
\newcommand{otherwise}[0]{\text{otherwise}}
\newcommand{when}[0]{ & }
\newcommand{next}[0]{ ,\\[0.8em] }
\newcommand{NEXT}[0]{ \\[1em] }
\newcommand{then}[0]{ \\[0.5em] }
\)

Unit Impulse Function
---------------------

$$ d(x) = \begin{cases}
  {1 \over \epsilon}  \when -{\epsilon \over 2} ≤ x ≤ {\epsilon \over 2} \next
  0                   \when \otherwise.
\end{cases} $$

Delta Function
--------------

$$ \delta(x) = \lim_{\epsilon\rightarrow 0} d_\epsilon (x) $$

$$ \ii \delta(x) dx = 1 $$

  * That is a special case of a sifting property (with \(g(x) = 1\) and \(x_i = 0\))

### Sifting Property

$$ \ii g(x) \delta(x - x_i) dx = g(x_i) $$


Unit Step Function
------------------

$$ u(x) = \begin{cases}
  0   \when x < 0 \next
  1   \when x ≥ 0.
\end{cases} $$

$$ \int_{-\infty}^x \delta(v) dv = u(x) $$

$$ \delta(x) = { d u(x) \over dx } $$

  * When \(x = 0\), \(\delta(x)\) does not exist.

PMF \(\rightarrow\) PDF
-----------------------

$$ F_X(x) = \sum_{x_i \in S_X} P_X(x_i) u(x-x_i) $$

$$ f_X(x) = \sum_{x_i \in S_X} P_X(x_i) \delta(x-x_i) $$

### Theorem

1. $$ P[X = x_0] = q $$
2. $$ P_X(x_0) = q $$
3. $$ F_X(x_0^+) - F_X(x_0^-) = q $$
4. $$ f_X(x_0) = q \delta(x_0) $$


Mixed Random Variable
---------------------

> \(X\) is a mixed RV \(\Leftrightarrow\) \(f_X(x)\) has zero, impulse, and finite value.

Example
-------

$$
\begin{align*}
F_Y(y) & = \begin{cases}
0                   \when y < 0       \next
1/3 + 2y/9          \when 0 ≤ y ≤ 3   \next
1                   \when y > 3.
\end{cases}

\NEXT

f_Y(y) & = \begin{cases}
\delta(y)/3 + 2/9   \when 0 ≤ y ≤ 3   \next
0                   \when \otherwise.
\end{cases}

\NEXT

\E[Y] & = \ii y(1/3) \delta(y) dy + \int_0^3 y(2/9) dy
\then & = 0 + 1 = 1
\end{align*}
$$

More on Derived Random Variables
--------------------------------

For \(Y = aX\),

1. $$ F_Y(y) = F_X(y /a) $$
2. $$ f_Y(y) = { 1 \over a } f_X({ y \over a }) $$

For \(Y = X + b\),

<ol start=3>
<li>$$ F_Y(y) = F_X(y - b) $$
<li>$$ f_Y(y) = f_X(y - b) $$
</ol>


Conditioning a Continuous RV
----------------------------

Definition:

$$
\begin{align*}
f_{X|B}(x) & = \begin{cases}
  { f_X(x) \over P[B] }   \when x \in B \next
  0                       \when \otherwise.
\end{cases}

\NEXT
E[g(X) | B] & = \ii g(x) f_{X|B}(x) dx
\end{align*}
$$



