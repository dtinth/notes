
3. Discrete Random Variable
========================

## Recap: Experiment

  * Composed of procedure and observation.
  * From observation, we get outcomes
  * From all outcomes, we get "sample space"
  * For event A, we apply math. probability model \(
\newcommand{otherwise}[0]{\text{otherwise}}
\newcommand{Var}[0]{\text{Var}}
\newcommand{Cov}[0]{\text{Cov}}
\newcommand{E}[0]{\text{E}} \)

## Random Variable

  * Example probability model: count sheep until fall asleep, observe the number of counted sheeps over fence.
      * There are infinitely many outcomes.
  * If we assign a number to each outcome in S, each number that we observe is called "Random Variable."
      * X = number of sheep
          * \( S_X = \{ 1, 2, 3, 4, \dots \}  \leftarrow  \text{range of }X \)
      * 2 traffic lights, observe the sequence of light.
          * \( S = \{ R_1R_2, R_1G_2, G_1R_2, G_1G_2 \} \)
      * Y = number of red light
          * \( S_Y = \{ 0, 1, 2 \} \)
              * no one-to-one mapping from \(S\) to \(S_Y\)
      * T = time consumption (2 minutes for each red light)
          * \( S_T = \{ 0, 2, 4 \} \)
  * A random variable, such as \(X\), is a function that maps each outcome \(s\) in \(S\) to a real number \(x = X(s)\)

### Types

  * Discrete random variables
      * \(X\) = number of data packets arrived at a station in a second.
      * \(Y\) = number of shuttle-cocks used in a badminton game.
      * \(S_X\) is countable \(\Rightarrow\) \(X\) is a discrete RV.
      * \(S_X\) is finite \(\Rightarrow\) \(X\) is a finite RV.
  * Continuous random variables
      * Z = number of minutes of a video clip in YouTube



## PMF

Experiment ⇒ Probability Model ⇒ Random Variables ⇒ PMF

  * Probability mass function P_X(x)
  * Theorem
      * \( P_X(x) ≥ 0 \)
      * \( \sum_{x \in S_X} P_X(x) = 1 \)
      * For event \(B \subset S_X\)
          * \( P[B] = \sum_{x \in B} P_X(x) \)

### Uniform RV

> Let \(X\) be the number of spots on the face of a fair dice roll.

$$
P_X(x) = \begin{cases}
  \frac{1}{j - k + 1};  & x = k, k+1, k+2, \dots, j \\
  0;                    & \otherwise
\end{cases}
$$

$$ E[X] = \frac{j + k}{2} $$

### Bernoulli RV

> Let \(Y\) be 1 if a single program test pass, otherwise 0.

$$
P_Y(y) = \begin{cases}
  1 - p;      & x = 0 \\
  p;          & x = 1 \\
  0;          & \otherwise
\end{cases}
$$

$$ E[Y] = p $$

### Geometric RV

> Let \(Z\) be the number of tests until a failure program, including the fail one.

  * Let \(p\) = probability that a certain program fail the test.
  * \( P[Z = 1] = p \)
  * \( P[Z = 2] = p (1-p) \)
  * \( P[Z = 3] = p (1-p)^2 \)
  * \( P[Z = 4] = p (1-p)^3 \)

$$
P_Z(z) = \begin{cases}
  p (1 - p)^{z - 1};  & z = 1, 2, 3, \dots \\
  0;                  & \otherwise
\end{cases}
$$

$$ E[Z] = \frac{1}{p} $$

### Binomial RV

> Let \(K\) be the number of failed programs in \(n\) tests.

$$
P_K(k) = \begin{cases}
  \binom{n}{k} p^k (1-p)^{n-k};   & k = 0, 1, 2, \dots, n \\
  0;                              & \otherwise
\end{cases}
$$

$$ E[K] = np $$


### Pascal RV

> Let \(J\) be the number of tests until find \(k\) failed programs.

$$
P_J(j) = \begin{cases}
  \binom{j - 1}{k - 1} p^k (1-p)^{j-k};   & j = k, k+1, k+2, \dots \\
  0                                       & \otherwise
\end{cases}
$$

$$ E[J] = \frac{k}{p} $$


### Poisson RV

> Let \(X\) be the number of arrivals per unit time, given the _average_
> number of arrivals per unit time \(\alpha\).

$$
P_X(x) = \begin{cases}
  \frac{ \alpha^x e^{-\alpha} }{x!};    & x = 0, 1, 2, \dots \\
  0;                                    & \otherwise
\end{cases}
$$

$$ E[X] = \alpha $$

## CDF

### Theorem

For a discrete random variable \(X\), with \(S_X = \{ x_1, x_2, \cdots \}\) and \(x_1 ≤ x_2 ≤ \dots\)

1.  \( F_X(-\infty) = 0\)
2.  \( F_X(\infty) = 1\)
3.  \( \forall x' ≥ x, F_X(x') ≥ F_X(x) \)
3.  \( F_X(x_i) - F_X(x_i - \epsilon) = P_X(x_i);\:
       x_i \in S_X , \epsilon = +\text{small number} \)
4.  \( F_X(x) = F_X(x_i);\: x_i ≤ x < x_{i + 1} \)
6.  \( F_X(b) - F_X(a) = P[a < X ≤ b];\: b ≥ a \)


## Average

RV → Average

  * Average of a RV
      * A single number that describes the RV.
      * An example of statistic.
  * What is statistic?
      * Numbers that collect all information of things under our interest.
  * Averages
      * \( E[X] = \mu_X = \sum_{x \in S_X}xP_X(x) \leftarrow \text{mean} \)
      * \( P_X(x_\text{mod}) ≥ P_X(x) \leftarrow \text{mode} \)
      * \( P[X < x_\text{med}] = P[X > x_\text{med}] \leftarrow \text{median} \)

## Derived Random Variable

$$ Y = g(X) $$

$$ P_Y(y) = \sum_{x:g(x)=y}P_X(x) $$

### Theorem

  * \( E[Y] = \sum_{x \in S_X}g[X] P_X(x) \)
  * \( E[aX + b] = aE[X] + b \leftarrow \text{linear transformation} \)
      * Examples: temperature conversion, adding scores to everyone
  * \( E[X - \mu_X] = 0 \)


## Variance and Standard Deviation

$$ \Var[X] = E[(X - \mu_X)^2] $$

$$ \sigma_X = \sqrt{\Var[X]} $$

### Theorems

* \( X = a \Rightarrow \Var[X] = 0 \)
* \( Y = X + b \Rightarrow \Var[Y] = \Var[X] \)
* \( Y = aX \Rightarrow \Var[Y] = a^2 \Var[X] \)


## Conditional PMF

$$P_{X|B}(x) = P[X=x|B]$$

### Theorem

* \( P_X(x) = \sum_{i=1}^nP_{X|B_i}(x)P[B_i] \)


## Joint PMF

$$P_{X,Y}(x, y) = P[X=x, Y=y]$$

$$S_{X,Y} = \{ (x, y) | P_{x,y}(x,y) > 0 \}$$

  * \( \sum_{x \in S_X} \sum_{y \in S_Y} P_{X,Y}(x,y) = 1 \)
  * \( P_{X,Y}(x, y) ≥ 0 \)

### Theorem

  * \( P[B] = \sum_{(x,y) \in B} P_{X,Y}(x, y) \)

### Marginal PMF

  * \( P_X(x) = \sum_yP_{X,Y}(x,y) \)
  * \( P_Y(y) = \sum_xP_{X,Y}(x,y) \)


## Derived RV of a Joint RV

$$ P_W(w) = \sum_{(x,y):g(x,y)=w} P_{X,Y}(x,y) $$

### Theorem

  * \( E[W] = \sum_{x \in S_X} \sum_{y \in S_Y} g(X,Y) P_{X,Y}(x,y) \)

## Variance

$$ \begin{align*}
\Var[X+Y] & = \Var[X] + \Var[Y] + 2E[(X-\mu_X)(Y-\mu_Y)] \\
          & = \Var[X] + \Var[Y] + 2\Cov[X,Y]
\end{align*}$$

## Covariance

$$ \begin{align*}
\Cov[X,Y] & = E[XY] - \mu_X \mu_Y \\
          & = r_{X,Y} - \mu_X \mu_Y
\end{align*} $$

  * \( \Cov[X,X] = \Var[X] \)
  * Covariance critically depend on unit measurement, so we need "dimensionless" measurement.


## Correlation

$$ r_{X,Y} = E[XY] $$

  * \( r_{X,Y} = 0 \Rightarrow X \text{ and } Y \text{ are orthogonal} \)
  * \( \Cov[X,Y] = 0 \Rightarrow X \text{ and } Y \text{ are uncorrelated} \)

### Correlation Coefficient

$$ \begin{align*}
\rho_{X,Y}  & = { \text{Cov}[X,Y] \over \sqrt{\text{Var}[X]\text{Var}[Y] } } \\
            & = { \text{Cov}[X,Y] \over \sigma_x \sigma_y }
\end{align*}$$

  * \( \rho_{X,Y} \in [-1, 1] \)
  * \( \rho_{X,Y} > 0 \) — positive relationship
  * \( \rho_{X,Y} < 0 \) — negative relationship


## Conditional Joint PMF

$$ P_{X,Y|B}(x,y) = \begin{cases} { P_{X,Y}(x,y) \over P[B]} & (x,y) \in B \\
0 & \text{otherwise} \end{cases} $$

$$ P_{X|Y}(x|y) = P[X=x|Y=y] $$
