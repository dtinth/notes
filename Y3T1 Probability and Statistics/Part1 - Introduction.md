
1. Introduction to Probability
===========================

## Basis

  * Based on Physical Property
      * Tossing a dice, a coin, buying lottery
  * Based on Knowledge
      * Snow in Thailand, webpage has been hacked \(
\newcommand{happyface}[0]{\text{☺}} \)

> __If a situation cannot exactly be replicated__, but __not chaotic__,
> then we can assign a __probability value__.


## Terms

  * __Definition__ — Logic of probability.
  * __Axiom__ — Truth, no proof needed.
  * __Theorem__ — Derived from definitions and axioms, proved by definition, axioms and other theorems.


## Sets

  * Basic mathematics for probability.
  * Group of interesting things.
  * Each element in a set is called a __member__ of a set.
  * \( A = B \Leftrightarrow A \subset B \text{ and } B \subset A \)
  * Let \(A\) be any set,
      * \( A \subset S \leftarrow \text{universal set} \)
      * \( \varnothing \subset A \)

### Properties

  * \(A_1, A_2, \dots, A_J\) are __mutually exclusive__ when \( A_i \cap A_j = \varnothing \) for \(i \neq j\)
  * \(A\) and \(B\) are disjoint when \( A \cap B = \varnothing \)
  * \(A_1, A_2, \dots, A_J\) are __collectively exhaustive__ when \( \cup_{i=1}^n A_i = S \)
  * __De Morgan's Law__ — \( (A \cup B)^c = A^c \cap B^c \)

## Experiment

  * Method for finding some facts/conclusions.
  * Composed of __procedure__ and __observation__.
  * Why? Because of __uncertainty__.
  * There are many concerns. Need a __model__ to capture the important part.
  * We can make multiple observations from same procedure.
      * Same procedure but different observations → different experiment.

## Definitions in Probability

  * __Outcome__: any possible observation
  * __Sample space__ (\(S\)): _finest-grain_, _mutually exclusive_, _collectively exhaustive_ set of _outcomes_
      * _finest grain_: each outcome is different
      * _mutually exclusive_: if one outcome occurs, other will not occur. non-overlapping.
      * _collectively exhaustive_: every outcome must be in sample space.
  * __Event__ (\(E\)): set of _outcomes_
      * \( E \subset S \)
  * __Event Space__: _mutually exclusive_, _collectively exhaustive_ set of _events_

### Theorem on Event Space

For an event space \(B = \{ B_1, B_2, \dots \} \), and any event \(A \in S\),

Let \(C_i = A \cap B_i\).

  * For \(i \neq j\), the events \(C_i\) and \(C_j\) are mutually exclusive.
  * \( A = C_1 \cup C_2 \cup \cdots \)

## Set vs Probability

<table>
  <tr><th>Set Algebra<th>Probability
  <tr><td>Set<td>Event
  <tr><td>Universal Set<td>Sample Space
  <tr><td>Element<td>Outcome
</table>


## Probability of an Event

\( P[\happyface] \) is a function that maps event in the sample space to a real number.

### Definition

Do \(n\) trials of an experiment. The number of occurences of event \(A\) is \(N_A(n)\).

$$ P[A] = \lim_{n \rightarrow \infty} \frac{N_A(n)}{n} $$

### Axioms

  * \( P[A] ≥ 0 \)
  * \( P[S] = 1 \)
  * For mutually exclusive events \( A_1, A_2, \dots, A_n \),
    \( P[A_1 \cup A_2 \cup \cdots \cup A_n] = P[A_1] + P[A_2] + \cdots + P[A_n] \)

### Theorems

  * \( A \text{ and } B \text{ are disjoint} \Rightarrow P[A \cup B] = P[A] + P[B] \)
  * \( (B = B_1 \cup B_2 \cup \cdots \cup B_n) \text{ and }
    (i \neq j \rightarrow B_i \cap B_j = \varnothing) \Rightarrow P[B] = \sum_{i = 1}^n P[B_i] \)
  * \( P[\varnothing] = 0 \)
  * \( P[A^c] = 1 - P[A] \)
  * \( P[A \cup B] = P[A] + P[B] - P[A \cap B] \)
  * \( A \subset B \Rightarrow P[A] ≤ P[B] \)

### Equally Likely

Let \(S = \{ s_1, s_2, \dots, s_n \}\).

If each outcome is __equally likely__, then \(P[s_i] = \frac{1}{n}; 1 ≤ i ≤ n\).