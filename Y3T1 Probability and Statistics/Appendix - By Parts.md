How to Integrate by Parts
=========================

<p class="text-center">
<a href="http://xkcd.com/1201/"><img src="http://imgs.xkcd.com/comics/integration_by_parts.png"  alt="Integrate by Parts" /></a>
</p>

Example
-------

$$
\int x e^x dx = ?
$$

Pick \(f(x)\) and \(g(x)\):

$$
\begin{align*}
  f(x) & = x \\
  g(x) & = e^x
\end{align*}
$$

Therefore,

$$
\begin{align*}
  u = & x \\
  dv = & e^x dx
\end{align*}
$$

So the problem becomes

$$
\int udv = ?
$$

But we know that

$$
\begin{align*}
  d(uv) & = vdu + udv \\
  \therefore udv & = d(uv) - vdu
\end{align*}
$$

So,

$$
\begin{align*}
  \int udv & = \int(d(uv) - vdu) \\
           & = \left( \int d(uv) \right) - \left( \int vdu \right) \\
           & = uv - \int vdu
\end{align*}
$$

But we only know \(u\) and \(dv\)... But finding \(du\) and \(v\) is trivial.

$$
\begin{align*}
  u & = x \\
  \therefore du & = dx \\[1em]
  dv & = e^x dx \\
  \therefore v & = e^x
\end{align*}
$$

Substitute it in,

$$
\begin{align*}
  \int udv & = uv - \int vdu \\
           & = xe^x - \int e^x dx \\
           & = xe^x - e^x + C \\
           & = (x-1)e^x + C \\
  \therefore \int x e^x dx
           & = (x-1)e^x + C
\end{align*}
$$

