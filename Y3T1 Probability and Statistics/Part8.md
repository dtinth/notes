

Point Estimates
------

* Parameters \(\theta\)
    * Unknown probability distribution
    * The real thing, but we cannot find it
* Statistics \(\hat\theta\)
    * Property of known sample of population
    * Can be used to estimate unknown parameters
    * Guess to represent the real thing; it is the best guess of the parameters

| Parameters \(\theta\) | Statistics \(\hat\theta\) |
| --------------------- | ------------------------- |
| Mean \(\mu\)             | Variance \(\bar X\)        |
| Sample mean \(\sigma^2\) | Sample variance \(S^2\)    |


Statistics
----------

| \(n\) random variables | A given data set |
| ---------------------- | ---------------- |
| $$ X_1, X_2, X_3, \dots, X_n $$ | $$ x_1, x_2, x_3, \dots, x_n $$ |
| $$ \bar X = \frac{X_1 + X_2 + \cdots + X_n}{n} $$ | $$ \bar x = \frac{x_1 + x_2 + \cdots + x_n}{n} = \hat\mu $$ |
| $$ S^2 = \frac{\sum_{i=1}^{n} (X_i - \bar X)^2}{n-1} $$ | $$ s^2 = \frac{\sum_{i=1}^{n} (x_i - \bar x)^2}{n-1} = \hat\sigma^2 $$ |


Estimation
----------

There may be more than one sensible point estimations. The point estimates of __mean__ of distribution \(\mu\):

* Sample mean \(\bar x\)
    * usually, smaller variance
    * unbiased
* Sample trimmed mean
    * more robust to outliers
    * biased when data observation is not symmetric

### An Unbiased Point Estimate

| Unbiased Point Estimate | Bias |
| ----------------------- | ---- |
| $$ E(\hat\theta) = \theta $$ | $$ \text{Bias} = E(\hat\theta) - \theta $$ |

### Unbiased Sample Variance

$$ S^2 = \frac{\sum_{i=1}^n (X_i - \bar X)^2}{n - 1} $$