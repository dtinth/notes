
2. Conditional Probability
=======================

$$ P[A|B] = \frac{P[AB]}{P[B]} ;\: P[B] > 0 $$

## Theorems

* \( P[A|S] = \frac{P[AS]}{P[S]} = P[A] \)

### Another Theorem

Let \(B_1, B_2, \dots, B_n\) be _mutually exclusive_ and _collectively exhaustive_ events.

Therefore, \(A = A \cap S = A \cap (B_1 \cup B_2 \cup \cdots \cup B_n)\)

$$
\begin{align*}
P[A]  & = P[A \cap B_1] + P[A \cap B_2] + \cdots + P[A \cap B_n] \\
      & = \sum_{i=1}^n P[A \cap B_i] \\
      & = \sum_{i=1}^n P[A|B_i] P[B_i] ;\: B_i > 0
\end{align*}
$$


### Bayes' Theorem

$$
P[B|A] = \frac{P[BA]}{P[A]} = \frac{P[AB]}{P[A]} = \frac{P[A|B]P[B]}{P[A]}
$$


## Independent Events

Events \(A\) and \(B\) are __independent__ iff...

$$ \(\Leftrightarrow\) \(P[AB] = P[A]P[B]\) $$

Therefore,

  * \(P[A|B] = P[A]\)
  * \(P[B|A] = P[B]\)

### Theorem

  * \( A \text{ and } B \text{ are independent} \Rightarrow
    P[A|B] = \frac{P[AB]}{P[B]} = \frac{P[A]P[B]}{P[B]} \)

> Make sure you know the difference between __independent__ and __disjoint__ events.

### With more than 2 events

Events \(A\), \(B\) and \(C\) are independent iff __all of these__ are true:

  * \(A\) and \(B\) are independent.
  * \(A\) and \(C\) are independent.
  * \(B\) and \(C\) are independent.
  * \(P[ABC] = P[A] P[B] P[C]\)


## Counting

  * __k-permutation__: \( (n)_k = \frac{n!}{(n - k)!} \)
      * Choose without replacement.
  * __Choose with replacement__: \( n^k \)
  * __k-combination__: \( \binom{n}{k} = \frac{n!}{k!(n-k)!} \)


## Independent Trials

  * Perform repeated, independent trials.
  * Let \(p\) be the probability of a successful trial, and \(1-p\) of a failure trial.
  * Let \(S_{k,n}\) be the event that there are \(k\) successes in \(n\) trials.

$$
  P[S_{k,n}] = \binom{n}{k} p^k (1-p)^{n-k}
$$