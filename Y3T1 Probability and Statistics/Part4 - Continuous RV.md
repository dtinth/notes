<style>
ol { list-style: hiragana-iroha; }
</style>

4. Continuous Random Variable
==========================

## Continuous Set

  * Uncountables
  * Can be represented using ranges.
      * (a, b)
      * (a, b]
      * [a, b)
      * [a, b]
  * Can be infinite
      * \( (-\infty, \infty) \)
  * Probability of each individual outcome is zero. We are interested in probability of "ranges".
  * CDF ⇔ PDF (Probability density function) \(
 
\newcommand{ii}[0]{\int_{-\infty}^\infty}
\newcommand{Var}[0]{\text{Var}}
\newcommand{E}[0]{\text{E}}
\newcommand{otherwise}[0]{\text{otherwise}}
\newcommand{when}[0]{ & ; \: }
\newcommand{next}[0]{ \\[0.8em] }
\)

### Theorem

  * \( F_X(-\infty) = 0 \)
  * \( F_X(\infty) = 1 \)
  * \( P[x_1 < x ≤ x_2] = F_X(x_2) - F_X(x_1) \)


## Probability Density Function

$$ f_X(x) = { dF_X(x) \over dx } $$

### Theorem
  * \( f_X(x) ≥ 0 \)
  * \( F_X(x) = \int_{-\infty}^{x} f_X(u) du \)


## PDF and CDF

  * PDF must be monotonically increasing.
  * \( P[x_1 < X ≤ x_2] = F_X(x_2) - F_X(x_1) \)
  * \( P[x_1 < X ≤ x_2] = \int_{x_1}^{x_2} f_X(x) dx \)
  * Approximately: \( P[x < X ≤ x+dx] = f_X(x) dx \)


## Expected Values, Variance

$$ \E[X] = \ii x f_X(x) dx $$

$$ \E[g(X)] = \ii g(x) f_X(x) dx $$

$$ \Var[X] = \ii (x - \mu_X)^2 f_X(x) dx $$

### Theorems

* \( \E[X - \mu_X] = 0 \)
* \( \E[aX + b] = a\E[X] + b \)
* \( \Var[X] = \E[X^2] - \E[X]^2 \)
* \( X = a      \Rightarrow  \Var[X] = 0 \)
* \( Y = X + b  \Rightarrow  \Var[Y]= \Var[X] \)
* \( Y = aX     \Rightarrow  \Var[Y] = a^2 \Var[X] \)

> Note: No score in exam if you don't write the labels for X and Y axis.
>
> Graph without labels are garbage; write labels correctly.

## Uniform Continuous RV

$$
\begin{align*}

f_X(x) & = \begin{cases}
\frac{1}{b - a}   \when a ≤ x < b      \next
0                 \when \otherwise
\end{cases}

\next

F_X(x) & = \begin{cases}
0                   \when x          \next
\frac{x - a}{b - a} \when a ≤ x < b  \next
1                   \when x >= 1
\end{cases}

\next

\E[X]   & = \frac{b + a}{2} \next
\Var[X] & = \frac{(b-a)^2}{12}

\end{align*}

$$


## Exponential Continuous RV

For \( \lambda > 0 \),

$$

\begin{align*}

f_X(x) & = \begin{cases}
  \lambda e^{-\lambda x} \when x ≥ 0   \next
  0                      \when \otherwise
\end{cases}

\next

F_X(x) & = \begin{cases}
  1 - e^{-\lambda x}  \when x ≥ 0       \next
  0                   \when \otherwise
\end{cases}

\next
\E[X]   & = \frac{1}{\lambda}     \next
\Var[X] & = \frac{1}{\lambda^2}   \next

\end{align*}
$$

  * To find probability that something is within __1 standard deviation__ is to find:
      * \( P[\mu_X - 1 \sigma_X ≤ X ≤ \mu_X + 1 \sigma_X] \)

### Geometric and Exponential RV Theorem

If \(X\) = Exponential RV with parameter \(a\)

Then \(K\) = \(\lceil X\rceil\) is a Geometric RV with parameter \(p = 1 - e^{-a}\)


# Continuous RV - After Midterm

## Gaussian RV

Where \(u \in \mathbb{R}\) and \(\sigma > 0\),

$$
\begin{align*}
f_X(x)  & = \frac{1}{\sqrt{2 \pi \sigma^2}} e^\frac{-(x - \mu)^2}{2\sigma^2}  \next
E[X]    & = \mu \next
\Var[X] & = \sigma^2
\end{align*}
$$

  * Also called Normal Random Variable, represented by \(N[\mu, \sigma^2]\).
  * \(f_X(x)\) is a bell shaped curve with 2 parameters: \(\mu\) and \(\sigma\)

### Theorem

If \(X\) is a Gaussian RV with parameters \(\mu\) and \(\sigma\),

then \(Y = aX + b\) is also a Gaussian RV, but with parameters \(a \mu + b\) and \(a \sigma\)


## Standard Normal RV

Standard Normal RV is a Gaussian RV with \(\mu = 0, \: \sigma = 1\):

$$
\begin{align*}
  f_Z(z)   & = \frac{1}{\sqrt{2 \pi}} e^\frac{-x^2}{2} \next
  \Phi(z)  & = \frac{1}{2 \pi} \int_{-\infty}^{z} e^\frac{-u^2}{2} du \\
           & = P[Z ≤ z]
\end{align*}
$$

  * \(\Phi(z)\) is the CDF (\(F_Z(z)\)).
  * \(\Phi(-z) = 1 - \Phi(z)\).


### Standard Normal Complementary
$$
\begin{align*}
  Q(z) & = P[Z > z] \\
       & = \frac{1}{\sqrt{2 \pi}} \int_z^\infty e^\frac{-u^2}{2} du \\
       & = 1 - \Phi(z)
\end{align*}
$$


### Percentile

$$ \text{k}^\text{th}\text{ percentile}: P(x) = 0.01k $$


### \(z_\alpha\) Notation

$$ P[Z ≥ z_\alpha] = \alpha $$

$$ z_\alpha = 100(1 - \alpha)^\text{th}\text{ percentile} $$


## Gaussian RV and Standard Normal Distributions

For a Gaussian RV with \(\mu\) and \(\sigma\),

$$
\begin{align*}
  F_X(x) & = \Phi(\frac{x - \mu}{\sigma}) \next
  P[a < X ≤ b] & = \Phi(\frac{b - \mu}{\sigma}) - \Phi(\frac{a - \mu}{\sigma})
\end{align*}
$$