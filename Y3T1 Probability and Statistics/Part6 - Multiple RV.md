<style>ol { list-style: hiragana-iroha; padding: 0 3em; }
ol.def { list-style: katakana-iroha; padding: 0 3em; }
ol li span.MathJax_Preview { display: none }</style>

Multiple Random Variable
========================

Joint CDF
---------

<ol class="def"><li>$$ F_{X,Y}(x, y) = P[X≤x, Y≤y]
\newcommand{ii}[0]{\int\limits_{-\infty}^\infty}
\newcommand{Var}[0]{\text{Var}}
\newcommand{Cov}[0]{\text{Cov}}
\newcommand{E}[0]{\text{E}}
\newcommand{otherwise}[0]{\text{otherwise}}
\newcommand{when}[0]{ & }
\newcommand{next}[0]{ ,\\[0.8em] }
\newcommand{NEXT}[0]{ \\[1em] }
\newcommand{then}[0]{ \\[0.5em] }
\newcommand{d}[1]{\operatorname{d}\!#1}$$
</ol>

<ol start=1>
<li>$$ 0 ≤ F_{X,Y}(x, y) ≤ 1 $$
<li>$$ F_{X}(x) = F_{X,Y}(x, \infty) $$
<li>$$ F_{Y}(y) = F_{X,Y}(\infty, y) $$
<li>$$ F_{X,Y}(-\infty, y) = F_{X,Y}(x, -\infty) = 0 $$
<li>$$ F_{X,Y}(x_1, y_1) > F_{X,Y}(x, y) \impliedby x_1 ≥ x \text{ and } y_1 ≥ y $$
<li>$$ F_{X,Y}(\infty, \infty) = 1 $$
</ol>

Joint PDF
---------

<ol class=def start=2>
<li>$$ F_{X,Y}(x, y) = \int\limits_{-\infty}^x \int\limits_{-\infty}^y f_{X,Y}(u, v) \d v \d u $$
<li>$$ f_{X,Y}(x, y) = { \partial^2 F_{X,Y}(x, y) \over \partial x \partial y } $$
</ol>

<ol start=7>
<li>$$ \begin{array}[l]{1}
  P[x_1 < X ≤ x_2, y_1 < Y ≤ y_2] \\
  \: \: \: = F_{X,Y}(x_2, y_2) - F_{X,Y}(x_2, y_1) - F_{X,Y}(x_1, y_2) + F_{X,Y}(x_1, y_1)
\end{array} $$
<li>$$ \forall (x, y), f_{X,Y}(x, y) ≥ 0 $$
<li>$$ \ii \ii f_{X,Y}(x, y) \d x \d y = 1 $$
<li>$$ P[A] = \iint\limits_A f_{X,Y}(x, y) \d x \d y $$
<li>$$ f_X(x) = \ii f_{X,Y}(x,y) \d y $$
<li>$$ f_Y(y) = \ii f_{X,Y}(x,y) \d x $$
</ol>

For \(W = g(X,Y)\),

<ol start=8>
<li>$$ F_W(w) = P[W ≤ w] = \iint\limits_{g(x,y)≤w} f_{X,Y}(x,y) \d x \d y $$
<li>$$ \E[W] = \E[g(X,Y)] = \ii\ii g(X,Y) f_{X,Y}(x,y) \d x \d y $$
<li>$$ \begin{align*} g(X,Y) = & g_1(X,Y) + \cdots + g_n(X,Y) \\
\Rightarrow \E[g(X,Y)] = & \E[g_1(X,Y)] + \cdots + \E[g_n(X,Y)] \end{align*} $$
<li>$$ \E[X+Y] = \E[X]+\E[Y] $$
</ol>

Conditioning Joint PDF
----------------------

<ol class=def start=4>
<li>$$
f_{X,Y|B}(x,y) = \begin{cases}
  \frac{f_{X,Y}(x,y)}{P[B]}  \when (x,y) \in B \next
0 \when \otherwise. \end{cases}
$$
<li>$$ f_{Y|X}(y|x) = { f_{X,Y}(x,y) \over f_X(x) } $$
<li>$$ f_{X|Y}(x|y) = { f_{X,Y}(x,y) \over f_Y(y) } $$
</ol>

<ol start=12>
<li>$$ f_{X,Y}(x,y) = f_{Y|X}(y|x) f_X(x) = f_{X|Y}(x|y) f_Y(y) $$
</ol>

Conditional Expected Value
--------------------------

<ol class=def start=7>
<li>$$ \E[X|Y=y] = \ii x \: f_{X|Y}(x|y) \d x   \impliedby f_Y(y) > 0 $$
<li>$$ \E[X|Y]   = E[X|Y=y]  \impliedby Y = y $$
<li>$$ \E[g(X,Y)|Y=y] = \ii g(x,y) f_{X|Y}(x|y) \d x   \impliedby f_Y(y) > 0 $$
</ol>

Independent RV
--------------

<p class="text-center">
\(X\) and \(Y\) are __independent__ \(\iff f_{X,Y}(x,y) = f_X(x) f_Y(y)\)
</p>

For __independent__ random variables \(X\) and \(Y\),

<ol start=13>
<li>$$ \E[g(X) \: h(Y)] = \E[g(X)] \: \E[h(Y)] $$
<li>$$ \Cov[X,Y] = 0 $$
<li>$$ \Var[X+Y] = \Var[X] + \Var[Y] $$
</ol>



Jointly Gaussian RV
-------------------

### Bivariate Gaussian RV

For \(\mu_1\) and \(mu_2\) \(\in R\), \(\sigma_1 > 0, \sigma_2 > 0\) and \(-1 ≤ \rho ≤ 1\),

<ol class=def start=10>
<li>$$
f_{X,Y}(x,y) = \frac{
  \operatorname{exp} \left[ {
    -\cfrac{
      (\cfrac{x-\mu_1}{\sigma_1})^2 -
      \cfrac{2\rho (x-\mu_1)(y-\mu_2)}{\sigma_1 \sigma_2} +
      (\cfrac{y-\mu_2}{\sigma_2})^2
    }{
      2(1-\rho^2)
    }
  } \right]
}{
  2 \pi \sigma_1 \sigma_2 \sqrt{1 - \rho^2}
}
$$
</ol>

<ol start=16>
<li>$$ f_X(x) = \frac{1}{\sigma_1 \sqrt{2\pi}} e^\frac{-(x-\mu_1)^2}{2\sigma_1^2} $$
<li>$$ f_Y(y) = \frac{1}{\sigma_2 \sqrt{2\pi}} e^\frac{-(y-\mu_2)^2}{2\sigma_2^2} $$
<li>$$ \begin{array}[l]{1}
f_{Y|X}(y|x) = \frac{1}{\tilde{\sigma_2} \sqrt{2 \pi}} e^\frac{-(y - \tilde{\mu_2}(x))^2}{2 \tilde{\sigma_2} ^2} \\
  \: \: \: \impliedby \tilde{\mu_2}(x) = \mu_2 + \rho \frac{\sigma_2}{\sigma_1} (x - \mu_1),
  \tilde{\sigma_2} = \sigma_2 \sqrt{(1 - \rho^2)} \end{array} $$
</ol>